////  HomeView.swift
//  SUI2_FoodOrderingFirebase
//
//  Created on 12/03/2021.
//  
//

import SwiftUI

struct HomeView: View {
    
    @StateObject var homeViewModel = HomeViewModel()
    
    var body: some View {
        
        ZStack {
            
            VStack(spacing: 10, content: {
                
                HStack(spacing: 15, content: {
                    
                    Button(action: {
                        withAnimation(.easeIn){
                            homeViewModel.showMenu.toggle()
                        }
                    }, label: {
                        Image(systemName: "line.horizontal.3")
                            .font(.title)
                            .foregroundColor(Color("Pink"))
                    })
                    
                    Text(homeViewModel.userLocation == nil ? "Loading ..." : "Deliver To")
                        .foregroundColor(.black)
                    
                    Text(homeViewModel.userAddress)
                        .font(.caption)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Pink"))
                    
                    Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                    
                })
                .padding([.horizontal, .top])
                
                Divider()
                
                HStack(spacing: 15, content: {
                    TextField("Search", text: $homeViewModel.search)
                    if homeViewModel.search != "" {
                        Button(action: {
                            
                        }, label: {
                            Image(systemName: "magnifyingglass")
                                .font(.title2)
                                .foregroundColor(.gray)
                        })
                        .animation(.easeIn)
                    }
                })
                .padding(.horizontal)
                .padding(.top,10)
                
                Divider()
                
                Spacer()
            })
            
            // Side Menu
            
            HStack {
                MenuView(homeData: homeViewModel)
                    .offset(x: homeViewModel.showMenu ? 0 : -UIScreen.main.bounds.width / 1.6, y: 10)
                
                Spacer(minLength: 0)
            }
            .background(Color.black.opacity(homeViewModel.showMenu ? 0.3 : 0).ignoresSafeArea())
            .onTapGesture {
                withAnimation(.easeIn){homeViewModel.showMenu.toggle()}
            }
            
            // Show Popup when no privileges to find location.
            if homeViewModel.noLocation {
                Text("Please Enable Location Acces Is Settings To Further Move On !!!")
                    .foregroundColor(.black)
                    .frame(width: UIScreen.main.bounds.width - 100, height: 120)
                    .background(Color.white)
                    .cornerRadius(10)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .background(Color.black.opacity(0.20).ignoresSafeArea())
            }
        }
        
        .onAppear(perform: {
            
            /* Need modifyng Info.plist
                + Privacy - Location When In Use Usage Description
                Please Allow To Read Your Location
             */
            
            homeViewModel.locationManager.delegate = homeViewModel
            print("Start")
            print()
        })
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
