////  MenuView.swift
//  SUI2_FoodOrderingFirebase
//
//  Created on 16/03/2021.
//  
//

import SwiftUI

struct MenuView: View {
    
    @ObservedObject var homeData: HomeViewModel
    
    var body: some View {
        
        VStack{
            Button {
                
            } label: {
                HStack(spacing: 15) {
                    Image(systemName: "cart")
                        .font(.title)
                        .foregroundColor(Color("Pink"))
                    
                    Text("Cart")
                        .fontWeight(.bold)
                        .foregroundColor(.black)
                    
                    Spacer(minLength: 0)
                }
                .padding()
            }
            
            Spacer()
            
            HStack {
                
                Spacer()
                
                Text("Version 0.1")
                    .fontWeight(.bold)
                    .foregroundColor(Color("Pink"))
            }
            .padding(10)
        }
        .frame(width: UIScreen.main.bounds.width / 1.6)
        .background(Color.white.ignoresSafeArea())
    }
}

