////  SUI2_FoodOrderingFirebaseApp.swift
//  SUI2_FoodOrderingFirebase
//
//  Created on 12/03/2021.
//  
//

import SwiftUI

@main
struct SUI2_FoodOrderingFirebaseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
