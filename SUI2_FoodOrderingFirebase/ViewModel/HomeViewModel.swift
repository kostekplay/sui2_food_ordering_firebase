////  HomeViewModel.swift
//  SUI2_FoodOrderingFirebase
//
//  Created on 12/03/2021.
//  
//

import SwiftUI
import CoreLocation

class HomeViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var search = ""
    @Published var locationManager = CLLocationManager()
    
    // Location details
    @Published var userLocation: CLLocation!
    @Published var userAddress = ""
    
    @Published var noLocation = false
    
    // Menu
    @Published var showMenu = false
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        switch manager.authorizationStatus {
            case .authorizedWhenInUse:
                print("AuthorizedWhenInUse")
                noLocation = false
                manager.requestLocation()
            case.denied:
                print("Denied")
                noLocation = true
            default:
                print("Unknown")
                noLocation = true
                locationManager.requestWhenInUseAuthorization()
        }
        
        print("locationManagerDidChangeAuthorization")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
        print("locationManager didFailWithError")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocation = locations.last
        print("locationManager didUpdateLocations")
        print("userLocation: \(userLocation!)")
        extractLocation()
    }
    
    func extractLocation() {
        CLGeocoder().reverseGeocodeLocation(userLocation) { (res, err) in
            
            guard let safeData = res else { return }
            
            var addres = ""
            
            addres += safeData.first?.name ?? ""
            addres += " ,"
            addres += safeData.first?.locality ?? ""
            
            self.userAddress = addres
            
            print("extractLocation")
            
        }
    }
}
