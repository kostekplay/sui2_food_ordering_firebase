////  ContentView.swift
//  SUI2_FoodOrderingFirebase
//
//  Created on 12/03/2021.
//  
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        HomeView()
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
